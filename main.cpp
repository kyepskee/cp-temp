#include <iostream>
#include <numeric>
#include <random>
#include <vector>
#include <algorithm>

using namespace std;

#define DEBUG
#ifdef DEBUG
#define dassert(x) assert(x)
#define df(...) printf(__VA_ARGS__)
#else
#define dassert(x)
#define df(...)
#endif

#define x first
#define y second
#define pb push_back
#define ir(x, a, b) ((a) <= (x) && (x) <= (b))
#define vec vector
#define foru(i, n) for (int i = 0; (i) < (n); ++(i))
#define fori(i, a, b) for (int i = a; i < (b); ++i)
#define all(x) (x).begin(), (x).end()

using ll = long long;

int main() {
    df("debug mode\n");
#ifndef DEBUG
    ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#endif
    return 0;
}
