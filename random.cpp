#include <iostream>
#include <chrono>
#include <numeric>
#include <random>
#include <vector>
#include <algorithm>

using namespace std;

// #define DEBUG
#ifdef DEBUG
#define dassert(x) assert(x);
#define df(...) printf(__VA_ARGS__)
#else
#define dassert(x)
#define df(...)
#endif

#define x first
#define y second
#define pb push_back
#define ir(x, a, b) ((a) <= (x) && (x) <= (b))
#define vec vector
#define foru(i, n) for (int i = 0; (i) < (n); ++(i))
#define fori(i, a, b) for (int i = a; i < (b); ++i)
#define all(x) (x).begin(), (x).end()

using ll = long long;

mt19937_64 gen;
ll r(ll a, ll b) { return gen()%(b-a+1) + a; }
ll ns() { 
    using namespace std::chrono;
    return duration_cast<nanoseconds>(high_resolution_clock::now().time_since_epoch()).count();
}

int main() {
    df("debug mode\n");
#ifndef DEBUG
    ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#endif
    gen.seed(ns());
    
    return 0;
}
