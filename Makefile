all: main
	./main <in

test: brute random main
	./test.sh

main: main.cpp
	# g++ -D_GLIBCXX_DEBUG -O3 -g -static -lm -Wall -Wextra -o main main.cpp
	g++ -O3 -g -static -lm -Wall -Wextra -o main main.cpp

brute: brute.cpp
	g++ -O3 -g -static -lm -Wall -Wextra -o brute brute.cpp

random: random.cpp
	g++ -O3 -g -static -lm -Wall -Wextra -o random random.cpp
