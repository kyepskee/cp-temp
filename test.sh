#!/usr/bin/env bash

set -e

i=0
max_tests=100000
while [ $i -le $max_tests ]
do
    ./random > randomin
    ./main <randomin >wrongout
    ./brute <randomin >correctout
    if diff --tabsize=1 -F --label --side-by-side --ignore-space-change correctout wrongout > dontshow; then
        echo "Accepted"
        # cat correctout
    else
        echo "Wrong"
        break
    fi
    i=$((i+1))
done
